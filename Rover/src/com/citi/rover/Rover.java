package com.citi.rover;

import java.security.DrbgParameters.NextBytes;

public class Rover {
    
    private int x, y, direction;
    private StringBuilder nextSteps = new StringBuilder("MMLMRM");
   

    public void receiveCommands(String cmd){
        for(int i=0; i<cmd.length(); i++){

            char nextCmd = cmd.charAt(i);

            if(nextCmd == 'L' || nextCmd == 'R'){
                nextSteps.append(nextCmd);
            }

            else if (Character.isDigit(nextCmd)){
                for (int j = 0; j < nextCmd - '0'; j++) {
					nextSteps.append("M");
				}
            }

            else{
                throw new IllegalArgumentException
                        ("Unrecognized command: " + nextCmd);
            }




        }
    }

    public void takeNextStep(){

        if(nextSteps.length() != 0){
        char nStep = nextSteps.charAt(0);
        nextSteps.deleteCharAt(0);
            if(nStep =='M'){
               if(direction == 0){
                ++y;
                
               }
               else if( direction == 1){
                ++x;
                
               }
               else if( direction == 2){
                --y;
                
               }
               else {
                   --x;
                   
               }

            }

            else if(nStep =='L'){
                direction = (direction + 3) % 4;
                
            }
            else if(nStep =='R'){
                direction = (direction + 1) % 4;
                
            }
            else{
                throw new IllegalArgumentException
                        ("Unrecognized command: " + nStep);
            }
        }
    }
        
        
        
    

    public int getX() {
        return x;
        
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }


    public boolean isBusy(){

        if(nextSteps.length() > 0){
            return true;
        }
        else{
        return false;
        }
    }
    
    public int getDirection(){
       
        return direction;
    }

    

   
}