public class TestAccount2 {
    
    public static void main(String[] args) {
   
        Account [] arrayOfAccounts = new Account[5];
        double[] amounts = {23,5444,2,345,34};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};

        for(int i=0; i<arrayOfAccounts.length; i++){
            arrayOfAccounts[i] = new Account(amounts[i],names[i]);

            System.out.println("Name: " + arrayOfAccounts[i].getName() + 
            "Balance: " + arrayOfAccounts[i].getBalance());

            Account.setInterestRate(0.2) ;

            arrayOfAccounts[i].addInterest()  ;

            System.out.println("Name: " + arrayOfAccounts[i].getName() + 
             "Balance: " + arrayOfAccounts[i].getBalance());


    }

    //boolean withdraw = arrayOfAccounts[3].withdraw();
    boolean withdraw = arrayOfAccounts[3].withdraw(500);

    if(withdraw){
        System.out.println();
    System.out.println("Success! Name: " + arrayOfAccounts[3].getName() + 
    "Balance: " + arrayOfAccounts[3].getBalance());
    }

    else{
        System.out.println();
    System.out.println("Failed transaction! Name: " + arrayOfAccounts[3].getName() + 
    "Balance: " + arrayOfAccounts[3].getBalance());
    }


}
}