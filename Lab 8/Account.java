public class Account {

    private double balance;
    private String name;

    private static double interestRate;
    
    public Account(double balance, String name) {
        this.balance = balance;
        this.name = name;
    }

    public Account() {

        this.name ="Ramsha";
        this.balance = 50;
    }


    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addInterest(){
        balance+= balance * interestRate;
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public boolean withdraw(double amount){

        if(balance >= amount){
            balance -= amount;
            return true;
        }

        else{
            return false;
        }
    }

    public boolean withdraw(){

        if(balance >= 20){
            balance -= 20;
            return true;
        }

        else{
            return false;
        }
    }






}