package com.conygre.spring.controller;

import com.conygre.spring.service.CompactDiscService;


import com.conygre.spring.entities.CompactDisc;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/compactdiscs")
@CrossOrigin // allows requests from all domains
public class CompactDiscController {

	// private Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private CompactDiscService service;

    //this is @GetMethod
    @RequestMapping(method = RequestMethod.GET)
	public Iterable<CompactDisc> getAllDiscs() {
		
		return service.getCatalog();
    }
    
    //this is @PostMethod
    @RequestMapping(method = RequestMethod.POST)
	public void addCd(@RequestBody CompactDisc disc) {
		service.addToCatalog(disc);
	}

}
