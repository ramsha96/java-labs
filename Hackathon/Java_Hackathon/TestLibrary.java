package Java_Hackathon;


/*
* Main class that creates an array of library books
* Demo for adding, removing, borrowing items
* not handling user id, borrowing term, penalties etc 
*/
public class TestLibrary {
    
    
    public static void main(String[] args) {
       
        
       Library [] items = {new Books("book1"), new Books("book2"), 
                            new Books ("book3"), new Books ("book4"),
                            new Books ("book5"), new Books ("book6"),
                            new CDs("CD1"), new CDs("CD2"), 
                            new CDs ("CD3"), new CDs ("CD4"),
                            new CDs ("CD5"), new CDs ("CD6"),
                            new DVDs("DVD1"), new DVDs("DVD2"), 
                            new DVDs ("DVD3"), new DVDs ("DVD4"),
                            new DVDs ("DVD5"), new DVDs ("DVD6"),
                            new Periodicals("Periodical1"), new Periodicals("Periodical2"), 
                            new Periodicals ("Periodical3"), new Periodicals ("Periodical4"),
                            new Periodicals ("Periodical5"), new Periodicals ("Periodical6"),

        };

        items[1].add();
        items[2].remove("book3");
        items[3].borrow("book3");

        items[6].borrow("cd1");

        items[18].borrow("Periodical1");

         
         
         
        
    //};
        
    }
        
}   

