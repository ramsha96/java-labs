package Java_Hackathon;

import java.util.HashMap;
import java.util.Map;

public class CDs extends Library

{
    private static Map<String, Integer> cds = new HashMap<String, Integer>();
    private String cdName;
    
    public CDs(String name) {
        this.cdName = name;

    }

    @Override
    public void add() {
        
        int code = (int) (100000 + Math.random() * 900000);
        cds.put(this.cdName, code);

        System.out.println(this.cdName + " added successfully added!");
    }

    @Override
    public void remove(String cd) {
        boolean success = false;
        for (Map.Entry<String, Integer> entry : cds.entrySet()) {
           
            if(entry.getKey() == cd){
                cds.remove(cd);
            System.out.println("CD: " + cd + ", Code: " + entry.getValue() + " removed from library.");
            success = true;
            }
        }

        if(!success){
            System.out.println("CD: " + cd + " does not exist in the library.");
        }

    }

    @Override
    public void borrow(String name) {
        boolean success = false;
        for (Map.Entry<String, Integer> entry : cds.entrySet()) {
           
            if(entry.getKey() == name){
                cds.remove(name);
            System.out.println("CD: " + name + ", code: " + entry.getValue() + " borrowed from library.");
            success = true;
            break;
            }
        }

        if(!success){
            System.out.println("CD: " + name + " does not exist in the library.");
        }

    }

    
}