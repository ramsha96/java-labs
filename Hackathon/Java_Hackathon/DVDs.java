package Java_Hackathon;

import java.util.HashMap;
import java.util.Map;

public class DVDs extends Library {

    private static Map<String, Integer> dvds = new HashMap<String, Integer>();
    private String dvdName;
    
    public DVDs(String name) {
        this.dvdName = name;

    }

    @Override
    public void add() {
        
        int code = (int) (100000 + Math.random() * 900000);
        dvds.put(this.dvdName, code);
        System.out.println(this.dvdName + " added successfully added!");
    }

    @Override
    public void remove(String dvd) {
        boolean success = false;
        for (Map.Entry<String, Integer> entry : dvds.entrySet()) {
           
            if(entry.getKey() ==dvd){
                dvds.remove(dvd);
            System.out.println("DVD: " + dvd + ", Code: " + entry.getValue() + " removed from library.");
            success = true;
            }
        }

        if(!success){
            System.out.println("DVD: " + dvd+ " does not exist in the library.");
        }
    }

    @Override
    public void borrow(String name) {
        boolean success = false;
        for (Map.Entry<String, Integer> entry : dvds.entrySet()) {
           
            if(entry.getKey() == name){
                dvds.remove(name);
            System.out.println("DVD: " + name + ", code: " + entry.getValue() + " borrowed from library.");
            success = true;
            break;
            }
        }

        if(!success){
            System.out.println("DVD: " + name + " does not exist in the library.");
        }

    }

}